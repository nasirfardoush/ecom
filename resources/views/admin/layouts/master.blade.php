@include('admin.layouts.include.header')
{{-- sidebar --}}
@include('admin.layouts.include.sidebar')


    <div class="grid_10">

        @yield('content')
    </div>
@include('admin.layouts.include.footer')