@extends('admin.layouts.master')

@section('content')
  <div class="box round first grid">
                <h2>Add New banner</h2>
            <div class="block">  
        
@if(Session::has('msg'))
           <h1>
               {{ Session::get('msg') }}
            </h1>
 @endif           
                 <form action="/admin/banner"  method="post" enctype="multipart/form-data">
                   {{ csrf_field() }}
                    <table class="form">           
                        <tr>
                            <td>
                                <label>Title</label>
                                <strong> ||{{ $errors->has('title') ? ' Title required' : '' }}</strong>
                            </td>
                            <td>
                                <input type="text" placeholder="Enter Post Title..." name="title" class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Button url</label>
                            </td>
                            <td>
                                <input type="text" name="button_url" />
                            </td>
                        </tr>       
                        <tr>
                            <td>
                                <label>Button text</label>
                            </td>
                            <td>
                                <input type="text" name="button_text" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <input type="file" name="image" />
                            </td>
                        </tr>
                       
                         <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
@endsection