{{--header section--}}
@include('layouts.include.header')
<!-- banner section -->
@yield('banner')
<!-- content section -->
@yield('content')
{{--footer section--}}
@include('layouts.include.footer');