<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
  
   protected $fillable = ['title','button_text','button_url','image'];
}
