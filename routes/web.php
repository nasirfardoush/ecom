<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();
Route::get('/', 'IndexController@index');
Route::get('/shirts', 'IndexController@shirt')->name('shart');
Route::get('/contact', 'IndexController@contact');
Route::get('/admin', 'HomeController@index')->name('home');

//Route for admin panel access
Route::get('admin/banner', 'BannerController@index');
Route::get('admin/banner/create', 'BannerController@create');
Route::post('admin/banner', 'BannerController@store');


